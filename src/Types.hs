{-# LANGUAGE ViewPatterns #-}


module Types ( Artist
             , AlbumName
             , ArtistAlbumNames
             , TrackInfo
             , Album
             , ArtistAlbums
             ) where 

import qualified Data.Time as Time
import qualified Data.Map  as Map


type Artist       = String
type AlbumName    = String
type ArtistAlbumNames = Map.Map Artist [AlbumName]
data TrackInfo    = Track { trackNumber           :: Int
                          , trackOriginalFilePath :: String
                          , trackStart            :: Time.UTCTime
                          , trackEnd              :: Time.UTCTime
                          , trackStartSector      :: Int
                          , trackEndSector        :: Int
                          , trackAccurate         :: Bool
                          , trackCopyOK           :: Bool
                          , trackPeakLevel        :: Double
                          , trackQuality          :: Double
                          , trackExtractSpeed     :: Double
                          } deriving (Show, Eq)

data Album = Alb { albumTracks      :: [TrackInfo]
                 , albumName        :: AlbumName
                 } deriving (Show, Eq)

albumAllAccurate :: Album -> Bool
albumAllAccurate = all trackAccurate . albumTracks

type ArtistAlbums = Map.Map Artist [Album]
