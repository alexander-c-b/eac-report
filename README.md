# *EAC Report*
*EAC Report* is a program written in the [Haskell language](https://www.haskell.org/) to parse and summarize the human-readable log output of Exact Audio Copy (EAC).

When digitizing music discs, Exact Audio Copy creates a tree structure with the following format:

-   `Library directory/`
    -   `Artist 1`
        -   `Album A`
            -   `Track1.flac`
            -   `Track2.flac`

                ...

            -   **`Album A.log`**
            -   `Album A.jpg` (the album art)
        -   `Album B`

            ...
    -   `Artist 2`
        -   `Album A`

            ...

        -   `Album B`

            ...

The first task of *EAC Report* is to crawl this library to find every log file.  The program must also account for the fact that albums with multiple CDs correspondingly have multiple log files.  See [`src/Directories.hs`](./src/Directories.hs) for the implementation.

Next is parsing the log files; several data fields (listed most importantly in [`src/Types.hs`](src/Types.hs)) are extracted from each log file.  This task forms the bulk of the program, written in [`src/Parse.hs`](src/Parse.hs).  Exact Audio Copy's log format is quite complex, made to be more readable by humans than computers.  Here is an example from the soundtrack for Microsoft's *Monster Truck Madness*:

```
TOC of the extracted CD

     Track |   Start  |  Length  | Start sector | End sector 
    ---------------------------------------------------------
        1  |  0:00.00 | 25:04.29 |         0    |   112828   
        2  | 25:04.29 |  2:31.05 |    112829    |   124158   
        3  | 27:35.34 |  2:07.38 |    124159    |   133721   
        4  | 29:42.72 |  2:15.39 |    133722    |   143885   
        5  | 31:58.36 |  3:08.18 |    143886    |   158003   
        6  | 35:06.54 |  2:26.55 |    158004    |   169008   
        7  | 37:33.34 |  3:02.11 |    169009    |   182669   
        8  | 40:35.45 |  2:55.31 |    182670    |   195825   
        9  | 43:31.01 |  3:19.13 |    195826    |   210763   
       10  | 46:50.14 |  2:37.65 |    210764    |   222603   
       11  | 49:28.04 |  3:05.32 |    222604    |   236510   
       12  | 52:33.36 |  2:48.60 |    236511    |   249170   
       13  | 55:22.21 |  2:43.34 |    249171    |   261429   

Track  2

     Filename J:\Flac rips\Microsoft\Monster Truck Madness\02 Racing Music 1.wav

     Peak level 89.1 %
     Extraction speed 1.8 X
     Track quality 99.9 %
     Copy CRC 0456488A
     Track not present in AccurateRip database
     Copy OK

Track  3

     Filename J:\Flac rips\Microsoft\Monster Truck Madness\03 Racing Music 2.wav

     Peak level 89.1 %
     Extraction speed 2.6 X
     Track quality 100.0 %
     Copy CRC 39D9D303
     Track not present in AccurateRip database
     Copy OK

...
```

The parsing is accomplished using the [parser combinator](https://en.wikipedia.org/wiki/Parser_combinator) library [Parsec](https://hackage.haskell.org/package/parsec).  In a nutshell, a parser is built out of combining smaller parsers into larger parsers.  For instance, in the above example, one parser would be responsible for parsing a single track listing.  Another parser function takes this track-listing parser as an input; that function then repeatedly parses the track listings and gathers the data results.  The parser for the track listing itself uses parsers for individual data points, like the filename and "track quality".

Finally, once the results have been collected, [Pandoc](https://hackage.haskell.org/package/pandoc) is used to create a simple HTML report of faulty disc copies.
