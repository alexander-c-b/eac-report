module Types.Disc ( Disc(..), allAccurate ) where

import qualified    Types.Track     as Track
import              Types.Track     ( Track )
import              Data.Maybe      ( maybe )

data Disc = Disc { number :: Int
                 , tracks :: [Track]
                 } deriving (Show, Eq)


allAccurate :: Disc -> Bool
allAccurate = all (maybe False Track.isRipAccurate . Track.rest) . tracks
