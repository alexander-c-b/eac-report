{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ViewPatterns #-}
module Report ( exactAudioCopies ) where

import qualified    Data.Text           as T
import              Data.Text           ( Text )
import qualified    Data.Map.Strict     as Map
import              Data.Map.Strict     ( Map )

import              Data.Traversable    ( for )

import qualified    Text.Pandoc.Builder as P
import              Text.Pandoc.Builder ( Pandoc )

import qualified    Types.Album         as Album
import              Types.Album         ( Album )
import              Types.Basic         ( Artist, AlbumName )


exactAudioCopies :: Map Artist [Album]  -- ^ Music library
                 -> Pandoc              -- ^ Report of extracted audio copies
exactAudioCopies (filterExact -> filtered) =
    P.doc $ P.bulletList
    $ Map.foldrWithKey (\artist albums l -> toEntry artist albums : l) [] filtered

    where toEntry artist albums =
            P.para (P.text artist)
            <> P.bulletList (map (P.para . P.text . Album.name) albums)

filterExact :: Map Artist [Album] -> Map Artist [Album]
filterExact library =
  Map.filter (not . null) $ Map.map (filter Album.allAccurate) library
