module ToHtml ( toHtml ) where


import qualified    Data.Text                   as T
import              Data.Text                   ( Text )

import              Text.Pandoc.Builder         ( Pandoc )
import              Text.Pandoc.Class           ( runPure )
import              Text.Pandoc.Writers.HTML    ( writeHtml5String )
import              Text.Pandoc.Options         ( def )


toHtml :: Pandoc -> Text
toHtml = continue . runPure . writeHtml5String def
    where continue (Left  err) = T.pack $ show err
          continue (Right res) = res
