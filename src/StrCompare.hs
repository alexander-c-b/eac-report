module StrCompare ( strCompare ) where


import Data.List
import Data.Maybe
import Control.Applicative
import Data.Function        ( on )


head' :: [a] -> Maybe a
head' [] = Nothing
head' (x:_) = Just x


prefixes :: [String]
prefixes = [ "The "
           , "the "
           , "THE "
           , "a "
           , "A "
           ]


stripPrefixes :: [String] -> String -> Maybe String
stripPrefixes prefixes string = 
    head' $ mapMaybe (`stripPrefix` string) prefixes


strCompare :: String -> String -> Ordering
strCompare = compare `on` stripped
    where stripped str = fromMaybe str $ stripPrefixes prefixes str
