module Types.Basic ( Artist
                   , AlbumName
                   ) where
import qualified Data.Text as T


type Artist    = T.Text
type AlbumName = T.Text
