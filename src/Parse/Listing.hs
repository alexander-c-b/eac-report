module Parse.Listing ( Listing(..)
                     ) where


import qualified    Data.Text   as T
import              Data.Text   ( Text )
import              Types.Time  ( Time )
import              Types.Track ( Rest )


-- * Information from an individual track listing
-- Track 1
--
--     Filename ...
--     ...
--
-- Track 2
data Listing = Listing { number        :: Int
                       , rest          :: Maybe Rest
                       } deriving (Show, Eq)
