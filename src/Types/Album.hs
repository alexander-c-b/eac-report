module Types.Album ( Album(..), allAccurate ) where

import              Data.Maybe  ( maybe )

import qualified    Types.Track as Track
import              Types.Track ( Track )
import qualified    Types.Disc  as Disc
import              Types.Disc  ( Disc )
import              Types.Basic ( AlbumName )


data Album = Album { name   :: AlbumName
                   , discs :: [Disc]
                   } deriving (Show, Eq)


-- | True if every track in the album has been ripped accurately; else False.
-- Also False if the track was aborted.
allAccurate :: Album -> Bool
allAccurate = all Disc.allAccurate . discs
