module Types.Track ( Track(..), Rest(..) ) where

import  System.FilePath ( FilePath )

import  Types.Time      ( Time )
import  Data.Text       ( Text )


data Track = Track { number        :: Int
                   , start         :: Time
                   , length        :: Time
                   , startSector   :: Int
                   , endSector     :: Int
                   , rest          :: Maybe Rest
                   } deriving (Show, Eq)


data Rest = Rest { filepath      :: Text
                 , suspicions    :: [(Time, Maybe Time)]
                 , peakLevel     :: Double
                 , extractSpeed  :: Double
                 , quality       :: Double
                 , copy          :: Text
                 , isRipAccurate :: Bool
                 , isCopyOk      :: Bool
                 } deriving (Show, Eq)
