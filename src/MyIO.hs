module MyIO ( readRipLog ) where


import System.IO    ( openFile
                    , IOMode(ReadMode)
                    , hSetEncoding
                    , utf16le
                    )
import Data.Text    ( Text )
import Data.Text.IO ( hGetContents )


readRipLog :: FilePath -- ^ log path
           -> IO Text  -- ^ log contents
readRipLog path = do file <- openFile path ReadMode
                     hSetEncoding file utf16le
                     hGetContents file
