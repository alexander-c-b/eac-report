module Types.Time ( Time(..)
                  ) where


import Text.Printf ( printf )


-- * Used for the start, length, etc. on the CD tracks
data Time = Time { hour    :: Int
                 , minute  :: Int
                 , seconds :: Double
                 } deriving (Eq)

instance Show Time where
    show (Time hour minute seconds) = 
        show hour <> ":" <> show minute <> ":" <> printf "%05.2g" seconds
        -- the printf format is 5 total characters, 0 padded, with 2 decimal 
        -- digits.
