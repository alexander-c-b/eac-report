module Directories ( getArtistsWithAlbums
                   , getArtistsWithNames ) where

import              System.FilePath     ( (</>)
                                        , splitPath
                                        , takeExtension
                                        )
import              System.Directory    ( listDirectory
                                        , getCurrentDirectory
                                        , withCurrentDirectory
                                        , doesDirectoryExist
                                        )
import              Control.Monad       ( filterM
                                        , (<=<)
                                        , (>=>)
                                        , forM
                                        )
import              Data.Functor        ( (<&>) )
import              Data.Traversable    ( sequenceA )
import              Data.List           ( sortBy
                                        , find )
import              Data.Maybe          ( mapMaybe )
import              Data.Either         ( rights, lefts, isLeft )
import              Data.Bifunctor      ( bimap, second )

import qualified    Data.Map.Strict     as Map
import              Data.Map.Strict     ( Map )
import qualified    Data.Text           as T
import              Data.Text           ( Text )

import              Types.Basic         ( Artist
                                        , AlbumName
                                        )
import              Types.Album         ( Album )
import qualified    Types.Album         as Album
import              Types.Disc          ( Disc )

import              StrCompare          ( strCompare )

import              MyIO                ( readRipLog )
import              Parse               ( extractDisc )


getArtistsWithAlbums :: FilePath               -- ^ library path
                     -> Map Artist [AlbumName]  -- ^ artists and album names
                     -> IO (Map Artist [Either [String] Album])
                        -- ^ artists and their albums or parsing errors

getArtistsWithAlbums path = Map.traverseWithKey $ 
    \artist albums -> withCurrentDirectory path $
        filterWithLogs <$>
            forM albums (listDirWithParent . (T.unpack artist </>) . T.unpack)
        >>= mapM (mapM readRipLog)
        <&> zipWith zipper albums . map (imap extractDisc)
  where zipper :: AlbumName -> [Either String Disc] -> Either [String] Album
        zipper name discs = case lefts discs of
                              []   -> Right $ Album.Album name $ rights discs
                              errs -> Left errs


getArtistsWithNames :: FilePath                    -- ^ library path
                    -> IO (Map Artist [AlbumName]) -- ^ artists and their albums
getArtistsWithNames path = withCurrentDirectory path $ do 
    dirs       <- fmap lastDirs $ getCurrentDirectory >>= listOnlyDirs 
    albumLists <- forM dirs $ fmap (map T.pack . lastDirs) . listOnlyDirs
    let artists = map T.pack dirs
    return $ Map.fromList $ zip artists albumLists

  where listOnlyDirs = filterM doesDirectoryExist <=< listDirWithParent
        lastDirs = sortBy strCompare . map (last . splitPath)


listDirWithParent :: FilePath -> IO [FilePath]
listDirWithParent parent = map (parent </>) <$> listDirectory parent


filterWithLogs :: [[FilePath]] -> [[FilePath]]
filterWithLogs = map $ filter $ (== ".log") . takeExtension


imap :: (Int -> a -> b) -> [a] -> [b]
imap f = snd . foldr (\a (n, l) -> (n + 1, f n a : l)) (0, [])
