module Parse.TOCRow ( TOCRow(..)
                    ) where


import Prelude    hiding ( length )
import Types.Time ( Time )


-- * Information from an individual row of the Table of Contents
data TOCRow = TOCRow { number      :: Int
                     , start       :: Time
                     , length      :: Time
                     , startSector :: Int
                     , endSector   :: Int
                     } deriving (Show, Eq)
