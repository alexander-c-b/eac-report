{-# LANGUAGE OverloadedStrings        #-}
{-# LANGUAGE DisambiguateRecordFields #-}
{-# LANGUAGE NamedFieldPuns #-}

module Parse ( extractDisc
             ) where

    {-
module Parse ( getArtistAlbums
               -- DEBUG
             , skipPreamble
             , test
             ) where
             -}

import qualified    Data.Text               as T
import              Data.Text               ( Text )
import qualified    Data.Map                as Map
import              Data.Map                ( Map )
import qualified    Data.ListLike           as LL

import              Control.Applicative     ( liftA2, liftA3 )
import              Data.Char               ( isSpace
                                            , isDigit
                                            )
import              Data.Functor            ( (<&>) )

import              Text.Parsec
import              Text.Parsec.Char

import qualified    Types.Disc              as Disc
import              Types.Disc              ( Disc )
import qualified    Types.Track             as Track
import              Types.Track             ( Track )
import qualified    Types.Time              as Time
import              Types.Time              (Time)
import qualified    Parse.TOCRow            as TOCRow
import              Parse.TOCRow            (TOCRow)
import qualified    Parse.Listing           as Listing
import              Parse.Listing           (Listing)

extractDisc :: Int  -- ^ Disc number
            -> Text -- ^ Ripping log contents
            -> Either String Disc -- ^ Either error message (Left)
                                  -- or Disc (Right)
extractDisc number log =
    case parseLog log of
      Left  err  -> Left  $ show err
      Right list -> Right $ Disc.Disc number $ map combine list


combine :: (TOCRow, Listing, DBAccurate) -> Track
combine (row, listing, accurate) =
  Track.Track 
      { number        = Listing.number        listing
      , start         = TOCRow.start          row
      , length        = TOCRow.length         row
      , startSector   = TOCRow.startSector    row
      , endSector     = TOCRow.endSector      row
      , rest          = r }
  where r = Listing.rest listing 
            <&> \x -> x { Track.isRipAccurate 
                          = Track.isRipAccurate x || accurate }

parseLog :: Text -> Either ParseError [(TOCRow, Listing, DBAccurate)]
parseLog = parse helper ""
    where helper = do skipPreamble
                      liftA3 zipAll
                        (spaces *> tableOfContents)
                        (spaces *> listings)
                        (spaces *> databaseAccuracies)
                      <* skipMany anyChar


type Parser a = Parsec Text () a
type DBAccurate = Bool


-- | Skips everything before the last Table of Contents in the report
--
-- There can be multiple reports in one, so this function skips all until the last
skipPreamble :: Parser ()
skipPreamble = do skipToLastOrNowhere $ text newReportStart
                  -- ^^^ get the final, actual report
                  skipTillSucceeds $ text "TOC of the extracted CD"
                  return ()

                  where newReportStart =
                          "------------------------------------------------------------"


-- | Parses the Table of Contents information
tableOfContents :: Parser [TOCRow]
tableOfContents = do
    text "Track |   Start  |  Length  | Start sector | End sector"
    spaces
    text "---------------------------------------------------------"
    spaces
    tocEntry `endBy` spaces


-- | Parses an individual row from the Table of Contents
tocEntry :: Parser TOCRow
tocEntry = do
    number      <- integral <* divider
    start       <- time     <* divider
    length      <- time     <* divider
    startSector <- integral <* divider
    endSector   <- integral
    return $ TOCRow.TOCRow { number, start, length, startSector, endSector }

    where divider = spaces >> char '|' >> spaces


listings :: Parser [Listing]
listings = listing `endBy` spaces

listing :: Parser Listing
listing = do
    text "Track" >> spaces

    number <- integral <* spaces

    endNow <- let p =     try (text "Copy aborted")
                      <|> try (lookAhead (text "Track") *> fail "Not aborted")
                      <|> (anyChar *> p)
               in option False $ try p *> return True
    if endNow then return $ Listing.Listing { number, rest = Nothing }
              else do
        
        -- parserTrace "Did not abort"
        filepath      <- surround   "Filename " tillLineEnd ""
        suspicions    <- parseSuspicions
        peakLevel     <- surround   "Peak level "       fractional  " %"
        extractSpeed  <- surround   "Extraction speed " fractional  " X"
        quality       <- surround   "Track quality "    fractional  " %"
        copy          <- surround   "Copy "             tillLineEnd ""
        isRipAccurate <- textExists "Accurately ripped" <* tillLineEnd <* spaces
        isCopyOk      <- textExists "Copy OK"           <* tillLineEnd <* spaces

        return $ Listing.Listing
                     { number
                     , rest = Just $
                         Track.Rest { filepath
                                    , suspicions
                                    , peakLevel
                                    , extractSpeed
                                    , quality
                                    , copy
                                    , isRipAccurate
                                    , isCopyOk } }

    where surround :: Text -> Parser a -> Text -> Parser a
          surround a parser b = between (text a) (text b) parser <* spaces
                                <?> T.unpack a

          parseSuspicions :: Parser [(Time, Maybe Time)]
          parseSuspicions =
              many $ between (text "Suspicious position ") spaces $
                  (,) <$> time <*> optionMaybe (text " - " >> time)


databaseAccuracies :: Parser [DBAccurate]
databaseAccuracies =
    try (skipTillSucceeds (text "Track | CTDB Status") 
         *> spaces
         *> table <*
             tillLineEnd)
    <|>
    return (repeat False) <* tillLineEnd

    where table    = accurate `endBy` spaces
          accurate = do
            digits *> spaces *> char '|' *> spaces
            char '(' *> skipTillSucceeds (char ')')
            spaces
            textExists "Accurately ripped" <* tillLineEnd




-- Auxiliary stuff
text :: Text -> Parser Text
text t = LL.mapM_ char t >> return t

manyT :: Parser Char -> Parser Text
manyT = manyTWorker LL.empty
    where manyTWorker text parser = do
            result <- optionMaybe $ try parser
            case result of
              Nothing -> return text
              Just c  -> manyTWorker (text `LL.snoc` c) parser

many1T :: Parser Char -> Parser Text
many1T parser = liftA2 LL.cons parser (manyT parser)

manyTillT :: Parser Char -> Parser end -> Parser Text
manyTillT p end = scan
    where scan = (end >> return LL.empty)
               <|>
                 liftA2 LL.cons p scan


-- Copied and modified from Text.Parsec.Prim
{-
manyAccumT :: (Char -> Text -> Text)
           -> Parser Char
           -> Parser Text
manyAccumT acc p =
    ParsecT $ \s cok cerr eok _eerr ->
    let walk xs x s' _err =
            unParser p s'
              (seq xs $ walk $ acc x xs)  -- consumed-ok
              cerr                        -- consumed-err
              manyErr                     -- empty-ok
              (\e -> cok (acc x xs) s' e) -- empty-err
    in unParser p s (walk LL.empty) cerr manyErr (\e -> eok LL.empty s e)
-}

-- | Parses as many digits as possible as a string
digits :: Parser String
digits = many1 (satisfy isDigit)

-- | Reads as many digits as possible into an integral number
integral :: (Integral a, Read a) => Parser a
integral = read <$> digits

fractional :: (Fractional a, Read a) => Parser a
fractional = fmap read $ digits <> option "" (string "." <> digits)

-- | Parses text of the form "05:13.50" (for example) into a Time
time :: Parser Time
time = do
    ints    <- many1 $ try $ integral <* char ':'
    seconds <- fractional
    case ints of
      [minute]       -> return $ Time.Time { hour = 0, minute, seconds }
      [hour, minute] -> return $ Time.Time { hour,     minute, seconds }
      _              -> fail $ "expected time of the form 0:0.0 or 0:0:0 or 0:0:0.0"

exists :: Parser a -> Parser Bool
exists a = (try a >> return True) <|> return False

textExists :: Text -> Parser Bool
textExists = exists . text

skipTillSucceeds :: Parser a -> Parser ()
skipTillSucceeds parser = anyChar `manyTill` try parser >> return ()

tillLineEnd :: Parser Text
tillLineEnd = anyChar `manyTillT` try endOfLine

skipToLastOrNowhere :: Parser a -> Parser ()
skipToLastOrNowhere parser = skipMany $ try $ skipTillSucceeds parser


zipAll :: [TOCRow] -> [Listing] -> [DBAccurate] -> [(TOCRow, Listing, DBAccurate)]
zipAll [] _ _ = []
zipAll _ [] _ = []
zipAll _ _ [] = []
-- Game CDs, such as Glenn Stafford/Warcraft II, have the first track as the
-- game's data.  This appears in the TOC, but NOT the listings.
-- Strangely, while the listings start at 2, the database info starts at 1 but
-- really means tracks 2+.
zipAll rows listings accuracies
  | l == t      = zip3 rows listings accuracies
  | l == t + 1  = zip3 (tail rows) listings accuracies
  | otherwise = error $ "Listing number "
                        <> show l
                        <> " does not match TOCRow number "
                        <> show t
                        <> case Listing.rest $ head listings of
                             Nothing -> ""
                             Just r  -> T.unpack $ Track.filepath r
                        <> ")"
  where l = Listing.number $ head listings
        t = TOCRow.number  $ head rows
