module Lib
    ( mainFunc
    ) where

import qualified    Data.Text.IO        as TIO
import qualified    Data.Map.Strict     as Map
import              Data.Map.Strict     ( Map )
import              Data.Either         ( partitionEithers )
import              Data.Bifunctor      ( first )
import              System.Environment  ( getArgs )

import              Directories         ( getArtistsWithAlbums
                                        , getArtistsWithNames )

import              Report              ( exactAudioCopies )
import              ToHtml              ( toHtml )


partitionMap :: Map k [Either a b] -> ([[a]], Map k [b])
partitionMap = Map.mapAccumRWithKey worker []
    where worker listOfLists key eithers = first (:listOfLists) $ partitionEithers eithers


mainFunc :: IO ()
mainFunc = getArgs >>= handleArgs

handleArgs :: [String] -> IO ()
handleArgs []     = putStrLn "Usage: music library_path"
handleArgs [path] = do 
    results <- getArtistsWithNames path
               >>= getArtistsWithAlbums path
    case first (concat . concat) (partitionMap results) of
      ([], mapping) -> TIO.putStr $ toHtml $ exactAudioCopies mapping
      (errs, _)     -> mapM_ putStrLn errs
      
handleArgs (path:_) = -- DEBUG
    getArtistsWithNames path >>= getArtistsWithAlbums path >>= print
